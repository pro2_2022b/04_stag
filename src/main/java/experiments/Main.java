package experiments;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import experiments.graphDataModel.Data;
import experiments.graphDataModel.Graph;
import experiments.stagDataModel.Akce;
import experiments.stagDataModel.Rozvrh;

import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws IOException
    {
        // TODO: stáhnout a deserializovat všechna JSON data

        Properties config = new Properties();
        Path configPath = Paths.get(System.getProperty("user.dir"),"app.config");
        config.load(new FileInputStream(configPath.toString()));

        int rokOd = Integer.parseInt(config.getProperty("rokOd"));
        int rokDo = Integer.parseInt(config.getProperty("rokDo"));
        String semestr = config.getProperty("semestr");
        String mistnost = config.getProperty("mistnost");

        ArrayList<Integer> values = new ArrayList<>();

        for(int rok = rokOd; rok <= rokDo; rok++)
        {
            String url = "https://stag-demo.uhk.cz/ws/services/rest2/rozvrhy/getRozvrhByMistnost" +
                    "?semestr=" + semestr +
                    "&rok=" + rok +
                    "&budova=J" +
                    "&mistnost=" + mistnost +
                    "&outputFormat=json";

            URL url1 = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            java.util.Scanner responseScanner = new java.util.Scanner(conn.getInputStream()).useDelimiter("\\A");
            String responseString = responseScanner.next();

            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            Rozvrh rozvrh = gson.fromJson(responseString, Rozvrh.class);

            int counter = 0;
            for(Akce akce : rozvrh.getRozvrhovaAkce())
            {
                counter += akce.getObsazeni();
            }
            values.add(counter);
        }

        ArrayList<Object> c3array = new ArrayList<Object>();
        c3array.add("Počet studentů");
        for (int value : values)
        {
            c3array.add(value);
        }
        Object[][] c3arrays = new Object[][]{c3array.toArray()};

        Graph graph = new Graph("#chart",new Data(c3arrays));

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String json = gson.toJson(graph);

        Path path = Paths.get(System.getProperty("user.dir"),"graph2.json");
        FileWriter fw = new FileWriter(path.toString());

        fw.write(json);
        fw.close();

        // TODO: vytvořit objekt pro export do JSONU
        // TODO: export do JSONU

    }
}
